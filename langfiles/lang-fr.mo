��          �      \      �  
   �     �  :   �  	   $     .     ;     @     F     L     ^     g     �     �     �     �  
   �     �     �     �  \  �  	   0     :  L   J     �     �     �     �     �     �     �     �     �                    '     8     M     S                                                    	                
                               Authorised Bank payment Base functionality for all online payment functionalities. Cancelled Card payment Date Email Error Gateway reference Received Recurring payment with %s Redirect Shopper Refused Request Response Shopper IP Shopper reference Token User Project-Id-Version: hopershop
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-08-09 11:42+0200
PO-Revision-Date: 2017-08-09 11:45+0200
Last-Translator: Paul de Rosanbo <paul.derosanbo@cantico.fr>
Language-Team: Cantico <paul.derosanbo@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: libpayment_translate;libpayment_translate:1,2;translate:1,2;translate
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: programs
 Autorisé Paiement banque Functionalité de base pour toutes les fonctionnalités de paiement en ligne Annulé Paiement carte Date Email Erreur Référence passerelle Reçu Paiement récurent avec %s Rediriger l'acheteur Refusé Requête Réponse IP de l'acheteur Référence acheteur Token Utilisateur 