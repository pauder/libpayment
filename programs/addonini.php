; <?php/*
[general]
name                          ="LibPayment"
version                       ="0.2.14"
addon_type                    ="LIBRARY"
mysql_character_set_database  ="latin1,utf8"
encoding                      ="UTF-8"
description                   ="Generic online payment functionality"
description.fr                ="Librairie partagée de paiement en ligne"
delete                        ="1"
longdesc                      =""
ov_version                    ="8.1.98"
php_version                   ="5.2.0"
addon_access_control          ="0"
db_prefix                     ="libpayment"
author                        ="Cantico ( support@cantico.fr )"
icon                          ="credit-card.png"
tags						  ="library,payment"
configuration_page	          ="main&idx=log.displaylist"


[addons]
widgets				="1.0.111"
LibTranslate	    ="1.5.2.04"

;*/ ?>