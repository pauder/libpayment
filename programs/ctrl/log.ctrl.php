<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */


class libpayment_CtrlLog extends libpayment_Controller
{
    private function getPage()
    {
        $W = bab_Widgets();
        
        $page = $W->BabPage();
        $page->addItemMenu('displayList', libpayment_translate('Transactions'), $this->proxy()->displayList());
        $page->addItemMenu('editSettings', libpayment_translate('Settings'), $this->proxy()->editSettings());
        
        return $page;
    }
    
    
    
    public function displayList($search = null)
    {
        if (!bab_isUserAdministrator()) {
            throw new bab_AccessException('Access denied');
        }
        
        require_once dirname(__FILE__).'/../ui/log.tableview.php';
        $list = new libpayment_LogTableView();
        
        require_once dirname(__FILE__) . '/../log.class.php';
        $paymentLogSet = new payment_logSet();
        
        $filter = isset($search['filter']) ? $search['filter'] : array();
        
        
        $list->addDefaultColumns($paymentLogSet);
        $list->setDataSource($paymentLogSet->select($list->getFilterCriteria($filter)));
        $list->setSortField('time:down');
        
        $page = $this->getPage();
        $page->setCurrentItemMenu(__FUNCTION__);
        $page->addItem($list->filterPanel());

        return $page;
    }
    
    
    /**
     * 
     * 
     */
    public function editSettings()
    {
        require_once dirname(__FILE__).'/../ui/settings.editor.php';

        $page = $this->getPage();
        $page->setCurrentItemMenu(__FUNCTION__);
        $page->addItem(new libpayment_SettingsEditor());
        $page->setTitle(libpayment_translate('Edit settings'));

        // add debug informations
        
        require_once dirname(__FILE__).'/../payment/alert.class.php';
        $alert = new libpayment_Alert();
        
        bab_debug('Amount '.$alert->getAmount().' Transactions '.$alert->getTransaction());
        
        return $page;
    }
    
    
    /**
     * @requireSaveMethod
     */
    public function saveSettings($settings = null)
    {
        $this->requireSaveMethod();
        
        $registry = libpayment_Registry();
        
        $registry->setKeyValue('limitAmountDuration', (int) $settings['limitAmountDuration']);
        $registry->setKeyValue('limitAmountTotal', (int) $settings['limitAmountTotal']);
        $registry->setKeyValue('limitTransactionDuration', (int) $settings['limitTransactionDuration']);
        $registry->setKeyValue('limitTransactionTotal', (int) $settings['limitTransactionTotal']);
        $registry->setKeyValue('email', $settings['email']);
        
        return true;
    }
    
    
    /**
     * Test alerts
     */
    public function alert()
    {
        require_once dirname(__FILE__).'/../payment/alert.class.php';
        $alert = new libpayment_Alert();
        $alert->checkTestPeriod();
    }
}