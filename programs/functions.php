<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/


define ( 'LIBPAYMENT_TPE_TBL', 'libpayment_tpe');




/**
 * Translate
 * @param string $str
 * @return string
 */
function libpayment_translate($str, $str_plurals = null, $number = null)
{


    if ($translate = bab_functionality::get('Translate/Gettext'))
    {
        /* @var $translate Func_Translate_Gettext */
        $translate->setAddonName('LibPayment');

        return $translate->translate($str, $str_plurals, $number);
    }

    return $str;
}



function libpayment_Controller()
{
    require_once $GLOBALS['babInstallPath'].'utilit/controller.class.php';
    require_once dirname(__FILE__).'/controller.class.php';
    return new libpayment_Controller();
}


/**
 * Get payment log from token
 * @param string $token
 * @return payment_log
 */
function libpayment_getPaymentLog($token)
{
    require_once dirname(__FILE__) . '/log.class.php';
    $paymentLogSet = new payment_logSet();
    $paymentLog = $paymentLogSet->get($paymentLogSet->token->is($token));
    

    if (!isset($paymentLog)) {
        throw new libpayment_Exception('No log found for the token');
    }
    
    return $paymentLog;
}


/**
 * @return bab_registry
 */
function libpayment_Registry()
{
    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/LibPayment/');
    
    return $registry;
}