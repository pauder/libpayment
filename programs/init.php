<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__).'/functions.php';

function LibPayment_getAdminSectionMenus(&$sUrl, &$sText)
{
	return false;
}

function LibPayment_getUserSectionMenus(&$sUrl, &$sText)
{
	return false;
}


function LibPayment_onSectionCreate(&$sTitle, &$sContent)
{
	return false;
}

function LibPayment_onDeleteAddon()
{
    $addon = bab_getAddonInfosInstance('LibPayement');
	$addon->unregisterFunctionality('Payment');
    
	return true;
}

function LibPayment_upgrade($sVersionBase, $sVersionIni)
{
	global $babDB;
	
	require_once $GLOBALS['babInstallPath'] . 'utilit/path.class.php';
	require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';

	$addon = bab_getAddonInfosInstance('LibPayment');

	$addon->registerFunctionality('Payment', 'payment.func.php');
	

	bab_functionality::get('LibOrm')->initMysql();
	$mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
	require_once dirname(__FILE__) . '/log.class.php';

	$logSet = new payment_logSet();
	$sql = $mysqlbackend->setToSql($logSet);

	require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
	$synchronize = new bab_synchronizeSql();

	$synchronize->fromSqlString($sql);


	$srcpath = new bab_Path(bab_getAddonInfosInstance('LibPayment')->getImagesPath(), 'logo');
	$destpath = new bab_Path('images/addons/LibPayment/logo');
	$destpath->createDir();
	foreach($srcpath as $file) {
		/*@var $file bab_Path */

		$newfile = clone $destpath;
		$newfile->push($file->getBasename());
		if (!$file->isDir())
		{
			copy($file->toString(), $newfile->toString());
		}
	}

	return true;


}
