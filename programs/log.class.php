<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

// Initialize mysql ORM backend.
bab_functionality::get('LibOrm')->initMySql();
ORM_MySqlRecordSet::setBackend(new ORM_MySqlBackend($GLOBALS['babDB']));




/**
 * @property ORM_StringField $token
 * @property ORM_IntField $user
 * @property ORM_DateTimeField $time
 * @property ORM_DecimalField $amount
 * @property ORM_TextField $payment
 * @property ORM_TextField $response
 * @property ORM_StringField $reference
 *
 * @method payment_log get()
 * @method payment_log newRecord()
 * @method payment_log[] select()
 */
class payment_logSet extends ORM_MySqlRecordSet
{
	public function __construct()
	{
		parent::__construct();

		$this->setPrimaryKey('id');

		$this->addFields(
			ORM_StringField('token'),
			ORM_IntField('user'),
		    ORM_DateTimeField('time'),
		    ORM_DecimalField('amount')
		      ->setDescription('Amount in payment'),
		    ORM_TextField('payment')
		      ->setDescription('Serialized payment object'),
		    ORM_TextField('response')
		      ->setDescription('The full response of the gateway for debug'),
		    ORM_StringField('reference') // PSP reference, should be extracted from response if possible
		      ->setDescription('Reference of the transaction given by the gateway')
		);


	}

}


/**
 * @property string $token
 * @property int $user
 * @property string $time
 * @property string $amount
 * @property string $payment
 * @property string $response
 * @property string $reference
 */
class payment_log extends ORM_MySqlRecord
{
    /**
     * @return libpayment_Payment
     */
    public function getPayment()
    {
        require_once dirname(__FILE__).'/payment/payment.class.php';
        require_once dirname(__FILE__).'/payment/cardpayment.class.php';
        require_once dirname(__FILE__).'/payment/recurringpayment.class.php';
        return unserialize($this->payment);
    }
}

