<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * Base class for all events related to payments.
 */
class libpayment_EventPayment extends bab_Event
{

    protected $payment = null;
    protected $amount = null;
    protected $transaction = null;
    protected $authorization = null;


    public function setResponseAmount($amount)
    {
    	$this->amount = $amount;
    	return $this;
    }


    public function getResponseAmount()
    {
    	return $this->amount;
    }


    public function setResponseAuthorization($authorization)
    {
    	$this->authorization = $authorization;
    	return $this;
    }


    public function getResponseAuthorization()
    {
    	return $this->authorization;
    }


    public function setResponseTransaction($trans)
    {
    	$this->transaction = $trans;
    	return $this;
    }


    public function getResponseTransaction()
    {
    	return $this->transaction;
    }

    /**
     * Sets the payment associated to this event.
     *
     * @return libpayment_EventPayment
     */
    public function setPayment(libpayment_Payment $payment)
    {
        $this->payment = $payment;
        return $this;
    }


    /**
     * Returns the payment associated to this event.
     *
     * @return libpayment_Payment
     */
    function getPayment()
    {
        return $this->payment;
    }
}





/**
 * This event will be fired when a payment has been successful.
 */
class libpayment_EventPaymentSuccess extends libpayment_EventPayment
{
}

/**
 * This event will be fired when a payment has been successful and the user get back to the site, 
 * this event may not occur if the user stay on the payment gateway confirmation page and close the window.
 */
class libpayment_EventPaymentUserReturn extends libpayment_EventPayment
{
}



/**
 * This event will be fired when a payment has been cancelled.
 * this event may not occur if the user stay on the payment gateway confirmation page and close the window.
 */
class libpayment_EventPaymentCancel extends libpayment_EventPayment
{
}



/**
 * This event will be fired when a payment triggered an error.
 */
class libpayment_EventPaymentError extends libpayment_EventPayment
{

	/**
	 * Exception error code (0)
	 * @var string
	 */
	public $errorCode = null;

	/**
	 * Payment gateway error string  (mandatory)
	 * @var string
	 */
	public $errorMessage;
	
	
	/**
	 * Payment gateway error code
	 * @var string
	 */
	public $response_code = null;
	
	
	/**
	 * Optional bank response code
	 * @var string
	 */
	public $bank_response_code = null;
	
	/**
	 * Optional payment mean
	 * @var string
	 * @example mastercard, visa ...
	 */
	public $payment_means = null;
	
	
	/**
	 * optional partial card number used
	 * @var string
	 */
	public $card_number = null;
	
	/**
	 * optional date and hour of the payment transaction request
	 * @var BAB_DateTime
	 */
	public $transmission_date = null;
}

