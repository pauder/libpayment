<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/log.class.php';
require_once dirname(__FILE__) . '/payment/payment.class.php';




class Func_Payment extends bab_functionality
{



	/**
	 * (non-PHPdoc)
	 * @see bab_functionality::getDescription()
	 */
	public function getDescription()
	{
		return libpayment_translate('Base functionality for all online payment functionalities.');
	}



	/**
	 * Creates a new libpayment_Payment object.
	 *
	 * @return libpayment_Payment
	 */
	public function newPayment()
	{
		$payment = new libpayment_Payment();
		return $payment;
	}
	
	
	/**
	 * Creates a new lipayment_CardPayment object.
	 *
	 * @return libpayment_CardPayment
	 */
	public function newCardPayment()
	{
	    require_once dirname(__FILE__).'/payment/cardpayment.class.php';
	    return new libpayment_CardPayment();
	}
	
	

	/**
	 * Creates a new lipayment_BankPayment object for SEPA direct debit.
	 *
	 * @return libpayment_BankPayment
	 */
	public function newBankPayment()
	{
	    require_once dirname(__FILE__).'/payment/bankpayment.class.php';
	    return new libpayment_BankPayment();
	}
	
	
	/**
	 * Create new authorisation object
	 * used by api with a direct payment call to service
     * 
     * @see Func_Payment::doPayment()
     * @see Func_Payment::authorizeRecurringPayment()
     * 
     * @return libpayment_Authorization
	 */
	public function newAuthorization()
	{
	    require_once dirname(__FILE__).'/payment/authorization.class.php';
	    return new libpayment_Authorization();
	}
	
	
	
	/**
	 * Create new refund object
	 * used by api with a direct payment call to service
	 *
	 * @see Func_Payment::doRefund()
	 *
	 * @return libpayment_Refund
	 */
	public function newRefund()
	{
	    require_once dirname(__FILE__).'/payment/refund.class.php';
	    return new libpayment_Refund();
	}
	
	
	
	/**
	 * Create new bank details object
	 * used by api as a payment mean to create a recurring ID for subsequent payments
	 *
	 *
	 * @return libpayment_BankDetails
	 */
	public function newBankDetails()
	{
	    require_once dirname(__FILE__).'/payment/bankdetails.class.php';
	    return new libpayment_BankDetails();
	}
	
	
	
	/**
	 * @return payment_logSet
	 */
	public function logSet()
	{
	    require_once dirname(__FILE__) . '/log.class.php';
	    return new payment_logSet();
	}



    /**
     *
     * @param libpayment_Payment $payment
     */
    protected function logPayment(libpayment_Payment $payment)
    {
        $logSet = $this->logSet();

        $log = $logSet->newRecord();

        $log->user = bab_getUserId();
        $log->token = $payment->getToken();
        $log->time = date('Y-m-d H:i:s');
        $log->payment = serialize($payment);
        $log->amount = $payment->getAmount();

        $log->save();

        return $this;
    }



	/**
	 *
	 * @param libpayment_Payment $payment
	 *
	 * @return string     An Html string containing a way to perform the payment for the end user.
	 */
    public function getPaymentRequestHtml(libpayment_Payment $payment)
	{
	    $this->logPayment($payment);

	    return '<a href="#">A sample link for payment</a>';
	}
	
	
	/**
	 * Do an authorization on a small amount
	 * save a recurring contract for the specified shopperReference (in the payment object)
	 * The payment given in parameter must contain payment mean information
	 * 
	 * @see self::doPayment()  Use for the subsequent payments, with a libpayment_RecurringPayment parameter
	 * 
	 * @throws Exception   The ideal case is to throw libpayment_AuthorisationException
	 *                     But the method may throw exception specific to the payment gateway
	 * 
	 * @param libpayment_Payment $payment
	 * @return libpayment_Authorization
	 */
	public function authorizeRecurringPayment(libpayment_Payment $payment)
	{
	    $this->logPayment($payment);
	    
	    throw new libpayment_Exception('Not implemented');
	}
	
	
	/**
	 * Do a direct payment with the API
	 * The Payment object must contain a payment mean
	 *
	 * @see libpayment_CardPayment
	 * @see libpayment_RecurringPayment
	 *
	 * @throws Exception   The ideal case is to throw libpayment_AuthorisationException
	 *                     But the method may throw exception specific to the payment gateway
	 *
	 * @param libpayment_Payment $payment A recurring payment or a card payment
	 *
	 * @return libpayment_Authorization
	 */
	public function doPayment(libpayment_Payment $payment)
	{
	    $this->logPayment($payment);
	     
	    throw new libpayment_Exception('Not implemented');
	}
	
	/**
	 * Do a refund on a previous payment with the API
	 * @param string               $token              The token of the previous payment
	 * @param libpayment_Payment   A new payment       With negative amount if the amount to refund is diffrent from the previous payment
	 * @return libpayment_Refund
	 */
	public function doRefund($token, libpayment_Payment $refund = null)
	{
	    $paymentLog = libpayment_getPaymentLog($token);
	    $payment = $paymentLog->getPayment();
	    
	    if (!isset($refund)) {
	        $refund = $this->newPayment();
	        $refund->setAmount(-1 * $payment->getAmount());
	        $refund->setCurrency($payment->getCurrency());
	    }
	    
	    if ($refund->getAmount() > 0) {
	        throw new libpayment_Exception('The refund amount must be negative');
	    }
	    
	    $this->logPayment($refund);
	    
	    return $this->refundAmount($payment, $paymentLog->reference, $refund);
	}
	
	
	/**
	 * Refund amount
	 * @param libpayment_Payment $payment  Original payment
	 * @param string $reference            Original payment reference stored via logResponse()
	 * @param libpayment_Payment $refund   The refund amount and currency 
	 * 
	 * @return libpayment_Refund
	 */
	protected function refundAmount(libpayment_Payment $payment, $reference, libpayment_Payment $refund)
	{
	    throw new libpayment_Exception('Not implemented');
	}
	
	
	/**
	 * Create new empty recurring contract
	 * @see self::getRecurringContractDetails()
	 * @return libpayment_RecurringContractDetail
	 */
	public function newRecurringContractDetail()
	{
	    require_once dirname(__FILE__).'/payment/recurringcontractdetail.class.php';
	    return new libpayment_RecurringContractDetail();
	}
	
	/**
	 * Get informations on recurring contracts
	 * the parameter is the shopper reference used in the creation of the contract 
	 * with the authorizeRecurringPayment method
	 * 
	 * @see self::authorizeRecurringPayment()
	 * 
	 * @param string $shopperReference
	 * 
	 * @return libpayment_RecurringContractDetail[]
	 */
	public function getRecurringContractDetails($shopperReference)
	{
	    return array();
	}
	
	
	/**
	 * Get a new recurring payment
	 * if the second argument is provided, a contract detail is associated by the PSP reference of the first authorization
	 * if no psp reference, the contract detail will be associated with the setRecurringContractDetail method
	 * 
	 * @param string $shopperReference         Shopper reference used on the first authorization (contract creation)
	 * @param string [$pspReference]           PSP reference of the first authorization, if no match, fallback to the first available
	 * 
	 * @return libpayment_RecurringPayment
	 */
    public function newRecurringPayment($shopperReference, $pspReference = null)
    {
        require_once dirname(__FILE__).'/payment/recurringpayment.class.php';
        $payment = new libpayment_RecurringPayment();
        $payment->setShopperReference($shopperReference);
        
        if (!isset($pspReference)) {
            return $payment;
        }
        
        $arr = $this->getRecurringContractDetails($shopperReference);
        foreach ($arr as $recurringContractDetail) {
            if (((string) $recurringContractDetail->firstPspReference) === (string) $pspReference) {
                $payment->setRecurringContractDetail($recurringContractDetail);
                return $payment;
            }
        }
        
        if (count($arr) > 0) {
            $payment->setRecurringContractDetail($arr[0]);
            return $payment;
        }
        
        throw new libpayment_NotFoundException(sprintf('No recurring contract detail found with shopperReference=%s pspReference=%s', $shopperReference, $pspReference));
    }



	/**
	 * @return array
	 */
	public function getTpeList()
	{
		return array();
	}
	
	
	/**
	 * Return false if the selected TPE is on a test backend
	 * default to false, or false if the payment backend has no idea of this information
	 * @return bool
	 */
	public function isTestTpe()
	{
	    return false;
	}



	/**
	 * @return libpayment_EventPaymentSuccess
	 */
	public function newEventPaymentSuccess()
	{
		require_once dirname(__FILE__) . '/payment.event.php';
		return new libpayment_EventPaymentSuccess();
	}
	
	/**
	 * @return libpayment_EventPaymentUserReturn
	 */
	public function newEventPaymentUserReturn()
	{
		require_once dirname(__FILE__) . '/payment.event.php';
		return new libpayment_EventPaymentUserReturn();
	}


	/**
	 * @return libpayment_EventPaymentCancel
	 */
	public function newEventPaymentCancel()
	{
		require_once dirname(__FILE__) . '/payment.event.php';
		return new libpayment_EventPaymentCancel();
	}


	/**
	 * @return libpayment_EventPaymentError
	 */
	public function newEventPaymentError()
	{
		require_once dirname(__FILE__) . '/payment.event.php';
		return new libpayment_EventPaymentError();
	}
}







class libpayment_Exception extends Exception
{
	private $gateway_message;
	private $commandline;


	public function setGatewayMessage($msg)
	{
		$this->gateway_message = $msg;
	}

	public function setCommandLine($msg)
	{
		$this->commandline = $msg;
	}


	public function getGatewayMessage()
	{
		return $this->gateway_message;
	}

	public function getCommandLine()
	{
		return $this->commandline;
	}
}


/**
 * Expected data not found on gateway
 */
class libpayment_NotFoundException extends libpayment_Exception
{
    
}



/**
 * Exception if authorisation failed on a transaction
 *
 */
class libpayment_AuthorisationException extends libpayment_Exception
{
	/**
	 * @var libpayment_Payment
	 */
	private $payment;
	
	
	
	/**
	 * @return libpayment_Payment
	 */
	public function getPayment()
	{
		return $this->payment;
	}
	
	/**
	 * Get the payment to use in the libpayment_EventPaymentSuccess from the log
	 * @return bool
	 */
	public function initPaymentFromToken($token)
	{
		$paymentLog = libpayment_getPaymentLog($token);
		
		if (!$paymentLog) {
			return false;
		}
		
		$this->payment = $paymentLog->getPayment();
		
		return true;
	}
	
	
	/**
	 * Payment gateway response code
	 * do not use the Exception code because gateway code can be a string and Exception code is an integer
	 * @var string
	 */
	public $response_code;
	
	
	/**
	 * Optional bank response code
	 * 
	 * equal to PSP reference ? 
	 * 
	 * @var string
	 */
	public $bank_response_code;
	
	/**
	 * Optional payment mean
	 * @var string
	 * @example mastercard, visa ...
	 */
	public $payment_means;
	
	
	/**
	 * partial card number used
	 * @var string
	 */
	public $card_number;
	
	/**
	 * Date and hour of the payment transaction request
	 * @var BAB_DateTime
	 */
	public $transmission_date;
	
}
