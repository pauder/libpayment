<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */


class libpayment_Alert
{
    protected function getLogSet()
    {
        require_once dirname(__FILE__).'/../log.class.php';
        return new payment_logSet();
    }


    /**
     * Send an email if
     */
    public function checkTestPeriod()
    {
        if (!$this->isAmountValid()) {
            $this->sendMessage(libpayment_translate('The total amount limit has been reached'));
        }
    
        if (!$this->isTransactionValid()) {
            $this->sendMessage(libpayment_translate('The number of transaction limit has been reached'));
        }
    }
    
    
    
    private function sendMessage($content)
    {
        require_once $GLOBALS['babInstallPath'].'utilit/mailincl.php';
        $registry = libpayment_Registry();
        
        $savedRecipients = $registry->getValue('email');
        
        if (null === $savedRecipients || '' === $savedRecipients) {
            return;
        }
        
        $email = bab_mail();
        if (!$email) {
            return;
        }
        
        $emails = explode(',', $savedRecipients);
        foreach ($emails as $recipient) {
            $email->mailTo(trim($recipient));
        }
        $email->mailSubject($_SERVER['HTTP_HOST']. ' Libpayment');
        $email->mailBody($content);
        return $email->send();
    }
    
    
    
    /**
     * Amount to test on alert
     * @return float
     */
    public function getAmount()
    {
        $registry = libpayment_Registry();
        $duration = (int) $registry->getValue('limitAmountDuration'); // hours
        
        if (0 === $duration) {
            return 0;
        }
        
        
        $logSet = $this->getLogSet();
        
        $now = new DateTime();
        $now->sub(new DateInterval('PT'.$duration.'H'));
        
        $res = $logSet->select($logSet->time->greaterThan($now->format('Y-m-d H:i:s')));
        
        $total = 0;
        foreach ($res as $log) {
            $total += (float) $log->amount;
        }
        
        return $total;
    }
    
    
    /**
     * Number of transactions to test on alert
     * @return int
     */
    public function getTransaction()
    {
        $registry = libpayment_Registry();
        $duration = (int) $registry->getValue('limitTransactionDuration'); // hours
        
        if (0 === $duration) {
            return 0;
        }
        
        $logSet = $this->getLogSet();
        
        $now = new DateTime();
        $now->sub(new DateInterval('PT'.$duration.'H'));
        
        $res = $logSet->select($logSet->time->greaterThan($now->format('Y-m-d H:i:s')));
        return $res->count();
    }
    
    
    
    /**
     * Test total amount validity rule
     * @return boolean
     */
    private function isAmountValid()
    {
        $registry = libpayment_Registry();
        
        $limit = $registry->getValue('limitAmountTotal');
        $total = $this->getAmount();
    
        return ($total < $limit);
    }
    
    
    /**
     * Test number of transaction validity rule
     * @return boolean
     */
    private function isTransactionValid()
    {
        $registry = libpayment_Registry();
        $limit = $registry->getValue('limitTransactionTotal');
        $total = $this->getTransaction();
    
        return ($total < $limit);
    }
    
}