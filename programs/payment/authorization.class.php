<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

/**
 * This is an object to represent an authorisation
 * used by api with a direct payment call to service
 * 
 * @see Func_Payment::doPayment()
 * @see Func_Payment::authorizeRecurringPayment()
 * 
 */
class libpayment_Authorization
{
    
    const AUTHORIZED            = 'Authorised';
    const REFUSED               = 'Refused';
    const ERROR                 = 'Error';              // This status will probably be handled before with an exception
    const CANCELLED             = 'Cancelled';
    const RECEIVED              = 'Received';
    const REDIRECT_SHOPPER      = 'RedirectShopper';
    
    /**
     * The Payment service provider reference
     * 16-digit unique reference associated with the transaction/the request.
     * This value is globally unique;
     * 
     */
    public $pspReference;
    

    /**
     * Authorization status
     */
    public $status;
    
    /**
     * Authorization code
     * when the status is libpayment_Authorization::AUTHORIZED
     */
    public $authorizationCode;
    
    /**
     * When the payment is not authorised, it is refused, or if an error occurs, 
     * this field holds the reason for the refusal or a description of the error.
     * @var string
     */
    public $refusalReason;
    
    
    /**
     * If the authorization has been captured
     * @var bool
     */
    public $isCaptured;
    
    /**
     * When payment is configured with an auto-capture
     * if the capture fail, the error is stored in this field. the payment can be confirmed afterward
     * @var string
     */
    public $captureError;
    
    
    /**
     * LibPayment token used in the authorization process
     * @var string
     */
    public $token;
    
    
    /**
     * Get internaionalized version of authorization status
     * @return string
     */
    public function getStatusTitle()
    {
        switch($this->status) {
            case self::AUTHORIZED:          return libpayment_translate('Authorised');
            case self::REFUSED:             return libpayment_translate('Refused');
            case self::ERROR:               return libpayment_translate('Error');
            case self::CANCELLED:           return libpayment_translate('Cancelled');
            case self::RECEIVED:            return libpayment_translate('Received');
            case self::REDIRECT_SHOPPER:    return libpayment_translate('Redirect Shopper');
        }
    }

}