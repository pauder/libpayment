<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */


/**
 * Bank details and billing address as a payment mean
 * Used only to store a recurring contract ID for future payment with the doPayment method
 *
 * @see Func_Payment::doPayment()
 * @see libpayment_RecurringContractDetail::$bank
 *
 */
class libpayment_BankDetails
{
    
    /**
     * Gateway ID for bank location
     * @var string
     */
    public $bankLocationId;
    
    
    /**#@+
     * bank account details
     * @var string
     */
    public $bankName;
    public $bankAccountNumber;
    public $bic;
    public $countryCode;
    public $iban;
    public $ownerName;
    public $bankCity;
    public $taxId;
    /**#@-*/
    
    /**#@+
     * Shopper informations, billing details
     * Values used to store bank informations as recurring details
     * @var string
     */
    public $shopperEmail;
    public $shopperReference;
    public $houseNumberOrName;
    public $street;
    public $city;
    public $stateOrProvince;
    public $country;    // billing address country
    public $postalCode;
    
    public $shopperFirstName;
    public $shopperLastName;
    public $shopperGender;
    public $dateOfBirth;
    public $entityType;
    public $nationality;  // entity country
    /**#@-*/
}