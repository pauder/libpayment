<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * SEPA direct debit payment
 * 
 */
class libpayment_BankPayment extends libpayment_Payment
{
    
    /**
     * @var libpayment_BankDetails
     */
    protected $bankdetails;
    
    
    /**
     * @param libpayment_BankDetails $bankdetails
     */
    public function setBankDetails(libpayment_BankDetails $bankdetails)
    {
        $this->bankdetails = $bankdetails;
        return $this;
    }
    
    
    /**
     * 
     * @return libpayment_BankDetails
     */
    public function getBankDetails()
    {
        return $this->bankdetails;
    }
    
    public function toString()
    {
        $str = parent::toString();
        $str .= "\n\n".libpayment_translate('Bank payment');
        return $str;
    }
}