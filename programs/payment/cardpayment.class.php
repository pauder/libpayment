<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * Credit card payment
 * 
 */
class libpayment_CardPayment extends libpayment_Payment
{
    
    /**
     * Clear card data
     * This probably require a PCI DSS Certification
     * @var libpayment_Card
     */
    protected $card;
    
    /**
     * Encrypted by javascript card data
     * @var string
     */
    protected $encryptedCard;
    
    /**
     * screen with and height for 3D secure infos
     * @var array
     */
    protected $screen;
    
    /**
     * 
     * @return libpayment_Card
     */
    public function newCard()
    {
        require_once dirname(__FILE__).'/card.class.php';
        return new libpayment_Card();
    }
    
    public function setCard(libpayment_Card $card)
    {
        $this->card = $card;
        return $this;
    }
    
    /**
     * 
     * @return libpayment_Card
     */
    public function getCard()
    {
        return $this->card;
    }
    
    /**
     * Set card data, encypted from javascript
     * @param string $data
     */
    public function setEncyptedCard($data)
    {
        $this->encryptedCard = $data;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getEncyptedCard()
    {
        return $this->encryptedCard;
    }
    
    public function setScreen($width, $height)
    {
        $this->screen = array($width, $height);
        return $this;
    }
    
    public function getScreen()
    {
        if (!isset($this->screen)) {
            return array(0, 0);
        }
        return $this->screen;
    }
    
    public function toString()
    {
        $str = parent::toString();
        $str .= "\n\n".libpayment_translate('Card payment');
        return $str;
    }
}