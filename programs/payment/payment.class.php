<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

/**
 * A payment with no payment mean information
 * to use with the gateway own payment page
 * 
 * @see Func_Payment::getPaymentRequestHtml()
 */
class libpayment_Payment
{
	/**
	 * @var float $amount
	 */
    protected $amount = null;

	/**
	 * @var string $currency
	 */
    protected $currency = null;

    /**
     * The email address of the customer.
     *
     * @var string $email
     */
   	protected $email = null;
   	
   	
   	/**
   	 * Customer IP
   	 * @var string
   	 */
   	protected $shopperIP = null;
   	
   	/**
   	 * Customer reference, used for recurring contract definition
   	 *
   	 * @var string
   	 */
   	protected $shopperReference = null;

    /**
     * A unique identifier for this payment.
	 *
     * @var string $token
     */
    protected $token = null;

    /* @var Serializable purchase */
    protected $purchase = null;
    
    /**
     * Free text string attached to query (shopping cart text description or number)
     * @var string
     */
    protected $text;


    const CURRENCY_EUR = 'EUR';
    const CURRENCY_USD = 'USD';
    const CURRENCY_CFA = 'CFA';
    const CURRENCY_CHF = 'CHF';
    const CURRENCY_GBP = 'GBP';
    const CURRENCY_CAD = 'CAD';
    const CURRENCY_JPY = 'JPY';
    const CURRENCY_MXP = 'MXP';
    const CURRENCY_TRL = 'TRL';
    const CURRENCY_AUD = 'AUD';
    const CURRENCY_NZD = 'NZD';
    const CURRENCY_NOK = 'NOK';
    const CURRENCY_BRC = 'BRC';
    const CURRENCY_ARP = 'ARP';
    const CURRENCY_KHR = 'KHR';
    const CURRENCY_TWD = 'TWD';
    const CURRENCY_SEK = 'SEK';
    const CURRENCY_DKK = 'DKK';
    const CURRENCY_KRW = 'KRW';
    const CURRENCY_SGD = 'SGD';



    public function __construct()
    {
        // LibPaymentCmCic support only 12 characters
    	$this->token = substr(uniqid(), -12);
    }


    /**
     * @param string $amount
     * @return libpayment_Payment
     */
    public function setAmount($amount)
    {
    	$this->amount = $amount;
    	return $this;
    }



	/**
	 * @return string
	 */
    public function getAmount()
    {
    	return $this->amount;
    }



	/**
	 * @param string $currency
	 * @return libpayment_Payment
	 */
    public function setCurrency($currency)
    {
    	$this->currency = $currency;
    	return $this;
    }


	/**
	 * @return string
	 */
    public function getCurrency()
    {
    	return $this->currency;
    }




	/**
	 * @param mixed $purchase
	 * @return libpayment_Payment
	 */
    public function setPurchase($purchase)
    {
    	$this->purchase = $purchase;
    	return $this;
    }


	/**
	 * @return mixed
	 */
    public function getPurchase()
    {
    	return $this->purchase;
    }
    
    
    /**
     * @param string $text
     * @return libpayment_Payment
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }
    
    
    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }



    /**
     * @param string $email
     * @return libpayment_Payment
     */
    public function setEmail($email)
    {
    	$this->email = $email;
    	return $this;
    }

	/**
	 * @return string
	 */
    public function getEmail()
    {
    	return $this->email;
    }
    
    
    
    /**
     * Shopper reference, ex: shopper full name
     * This is used for subsequent payments in recurring contracts
     * 
     * @param string $ref
     * @return libpayment_Payment
     */
    public function setShopperReference($ref)
    {
        $this->shopperReference = $ref;
        return $this;
    }
    
    /**
     * Customer reference, This is the field used for the recurring contract
     * This must be unique in your users base
     * 
     * @return string
     */
    public function getShopperReference()
    {
        return $this->shopperReference;
    }

    
    
    /**
     * @param string $email
     * @return libpayment_Payment
     */
    public function setShopperIp($ip)
    {
        $this->shopperIP = $ip;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getShopperIp()
    {
        return $this->shopperIP;
    }




    /**
     * @return string
     */
    public function getToken()
    {
    	return $this->token;
    }
    
    
    
    protected function getLogSet()
    {
        require_once dirname(__FILE__).'/../log.class.php';
        return new payment_logSet();
    }


    /**
     * Save in log a stringified response for debugging purpose
     * And optional reference for the transaction (PSP reference)
     * 
     * @param string $response
     * @param string $reference     payment service provider reference
     *
     * @return bool
     */
    public function logResponse($response, $reference = null)
    {
        $logSet = $this->getLogSet();
    
        $log = $logSet->get($logSet->token->is($this->token));
    
        if (!isset($log)) {
            throw new Exception('No log for this token '.$this->token);
        }
    
        $log->response = $response;
        $log->reference = $reference;
        
        $logResult = $log->save();
        
        require_once dirname(__FILE__).'/alert.class.php';
        $alert = new libpayment_Alert();
        $alert->checkTestPeriod();
        
        return $logResult;
    }
    
    
    
    protected function pair($name, $value)
    {
        return $name.': '.$value;
    }
    
    
    public function toString()
    {
        $values = array();
        
        if ($ip = $this->getShopperIp()) {
            $values[] = $this->pair(libpayment_translate('Shopper IP'), $ip);
        }
        
        if ($ref = $this->getShopperReference()) {
            $values[] = $this->pair(libpayment_translate('Shopper reference'), $ref);
        }
        
        if ($email = $this->getEmail()) {
            $values[] = $this->pair(libpayment_translate('Email'), $email);
        }
        
        $values[] = $this->getAmount().' '.$this->getCurrency();
        
        return implode("\n", $values);
    }
}

