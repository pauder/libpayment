<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * This is the class used to description a recurring contract
 * @see Func_Payment::getRecurringContractDetails()
 */
class libpayment_RecurringContractDetail
{

    /**
     * The recurring contract ID to use for subsequent payments
     * @see Func_Payment::doRecurringPayment()
     * @var string
     */
    public $id;
    
    
    /**
     * Payment mean text description
     * @example Mastercard
     * @var string
     */
    public $paymentMean;
    
    
    /**
     * The date when the recurring details were created.
     * @var BAB_DateTime
     */
    public $creationDate;
    
    
    /**
     * The card used when the recurring contract has been authorized
     * @see Func_Payment::authorizeRecurringPayment()
     * @var libpayment_Card
     */
    public $card;
    
    /**
     * If the payment mean is an bank account
     * @var libpayment_BankDetails
     */
    public $bank;
    
    
    /**
     * The Payment service provider reference of the first 
     * recurring payment that created the recurring detail.
     * @var string
     */
    public $firstPspReference;
    
    
    /**
     * Shopper reference
     * @var string
     */
    public $shopperReference;
    
    
    
    
    /**
     * 
     * @return libpayment_Card
     */
    public function newCard()
    {
        require_once dirname(__FILE__).'/card.class.php';
        return new libpayment_Card();
    }
    
    /**
     * Create new bank details object
     * used by api as a payment mean to create a recurring ID for subsequent payments
     *
     *
     * @return libpayment_BankDetails
     */
    public function newBankDetails()
    {
        require_once dirname(__FILE__).'/bankdetails.class.php';
        return new libpayment_BankDetails();
    }
    
    
}