<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

/**
 * This is an object to represent a refund
 * used by api with a direct payment call to service
 * 
 * @see Func_Payment::doRefund()
 * 
 */
class libpayment_Refund
{
    
    const ERROR                 = 'Error';      // This status will probably be handled before with an exception
    const RECEIVED              = 'Received';
    
    
    /**
     * Refund status
     */
    public $status;
    
    /**
     * The Payment service provider reference
     * 16-digit unique reference associated with the transaction/the request.
     * This value is globally unique;
     * 
     */
    public $pspReference;

    
    /**
     * When the refund fail, 
     * this field holds the reason for the refusal or a description of the error.
     * @var string
     */
    public $refusalReason;

    /**
     * LibPayment token used in the refund process
     * @var string
     */
    public $token;
    

    /**
     * Get internaionalized version of authorization status
     * @return string
     */
    public function getStatusTitle()
    {
        switch($this->status) {
            case self::ERROR:               return libpayment_translate('Error');
            case self::RECEIVED:            return libpayment_translate('Received');
        }
    }
}